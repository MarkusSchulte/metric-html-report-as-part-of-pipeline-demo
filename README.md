# Checkstyle HTML report as part of Bitbucket pipeline (demo)
[![Checkstyle count](https://s3.amazonaws.com/schultedev-xml-metrics-to-html/bitbucket/markusschulte/metric-html-report-as-part-of-pipeline-demo/bitbucket/checkstyle/count.svg)](https://s3.amazonaws.com/schultedev-xml-metrics-to-html/bitbucket/markusschulte/metric-html-report-as-part-of-pipeline-demo/bitbucket/checkstyle/index.html)

Demonstrating how to have a different metric HTML reports, such as
- [Checkstyle](https://github.com/checkstyle/checkstyle)/[PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)

as part of Bitbucket build pipeline.

## HowTo

- [bitbucket-pipelines.yml](bitbucket-pipelines.yml)

T.B.D.
